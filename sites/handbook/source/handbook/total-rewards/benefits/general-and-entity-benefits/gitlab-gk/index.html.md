---
layout: handbook-page-toc
title: "GitLab GK (Japan) Benefits"
description: "Discover GitLab's benefits for team members in Japan"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Specific to Japan Based Team Members

Team members in Japan have the following statutory state benefits available: medical, pension, and unemployment.

GitLab does not plan on offering additional private medical or pension benefits at this time due to the government cover.

GitLab will continue to review responses from the [Global Benefits Survey](/handbook/total-rewards/benefits/benefits-survey/#global-benefits-survey) as well as budgetary availability with respect to adding supplementary benefits in Japan. 

### Medical

All the Japanese team members will be covered by a statutory benefit known as Social Insurance. This health insurance plan will be managed by the National Health Insurance Association (Zenkoku-Kenkohoken-Kyokai or in short Kyokai Kenpo). 

### Pension

Team members registered to the Social Insurance are also registered with the Employee Pension system. This system provides a pension to members who have worked in Japan at least 25 years from the age of 60 years old. Pension benefits depend form individual career so there is no standard payments.

### Unemployment Insurance

* All the Japanese team members are entitled to the government’s unemployment insurance system, known as koyou hoken (unemployment insurance). Koyou hoken, also known as shitsugyou hokken (unemployment benefit), is meant to help members, including foreign members, who have recently been unemployed. 
* Members enrolling in unemployment insurance must be working at least 20 hours per week, and to expect to be employed for at least 31 days.
* Unemployment Insurance covers Unemployment benefits, Childcare Leave Allowance, Nursing Care Leave Allowance and Education/Training Allowance.

### Annual Health Check Up 

* Members covered by the Social general insurance system “Kyokai Kenpo” are entitled to a benefit of an annual health check up in any of the hospitals designated under “Kyokai Kenpo”. The Japanese team members can also have a checkup in a non-designated institution. 
* In both the cases the team members can expense up to 12000 JPY against the actual bills via Expensify. 
    * The invoice must be uploaded to Expensify to validate the total remiburseable amount. Please do not upload any personal medican data, only the invoice with the total amount due. If you have any questions or concerns please reach out to Total Rewards.  

### GitLab GK Leave Policy

#### Annual/Vacation Leave
A team member who has been continuously employed for six months is entitled to a minimum of ten days’ annual paid leave on the day after completing six months of employment. The entitlement increases by one day per year for the following two years and by two days per year thereafter, up to a maximum of 20 days per year. Annual paid leave runs concurrently with GitLab PTO. Team members must designate all vacation time taken as `Vacation` in PTO by Roots to ensure that vacation entitlement is properly tracked. Unused annual paid leave expires after two years if not used. Team members are not entitled to payment for unused vacation time upon termination of employment. 

#### Statutory Maternity Leave

The statutory entitlement for maternity leave is 14 weeks. The leave must start **6 weeks prior to the scheduled delivery date**. During the entire period of maternity leave, the team member is entitled to 2/3 of her base salary, and is covered by social insurance.

#### Statutory Parental Leave (Child Care Leave)

* The team members are entitled to unpaid leave until their child's first birthday (or second if certain conditions are met).
* Child care leave starts from the day after the maternity leave ends (i.e. 8 weeks after the birth date), to the day before the child reaches the age of 1.
* If both parents take child care leave, the leave is extended to when the child is 1 year and 2 months old.
* During the entire period of child care leave, the team member is entitled to 2/3 of their base salary, and is covered by labor insurance.
