---
layout: handbook-page-toc
title: UX Department
description: >-
  The GitLab UX department is comprised of four areas to support designing the
  GitLab product: UX Research, Product Design, Technical Writing, and UX
  Foundations
---

#### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Hello!

We're the GitLab User Experience (UX) department. We're comprised of four areas to support designing and building the GitLab product.

- [UX Research](/handbook/engineering/ux/ux-research/)
- [Technical Writing](/handbook/engineering/ux/technical-writing/)
- [Product Design](/handbook/engineering/ux/product-design/)
- UX Foundations - [Pajamas](https://design.gitlab.com)

Our goal is to make our product easy to use, supportive of contributions from the wider GitLab community, and built for a diverse global community. We want GitLab to be the easiest and most delightful product in its class.

We hope you find what you are looking for here. If you don't, please open an [issue](https://gitlab.com/gitlab-org/gitlab-design/issues/new) and give us feedback.

## How we work

- **We support all users from beginners to experts.** We believe that GitLab software should be unintimidating and accessible for a beginner, without oversimplifying important features for advanced users. We stay with users every step of the way to help them learn fast as a beginner and then become an expert over time.
- **We're building one product, together.** We're highly focused on ensuring that no matter how big our product gets, the entire experience stays cohesive, consistent, and interconnected.
- **We're humble facilitators of user experience design.** Everyone is a designer; everyone can contribute. We are not egotistical, moody experts who alone hold the keys to user delight. We encourage Product Managers, Engineers, and the wider GitLab community to contribute to creating an exceptional user experience.
- **We look for small changes and big impacts.** Sometimes the simplest, most boring solution is what is needed to make users successful. We want our UI to stay out of the user’s way. We work iteratively to make modest but valuable changes that make users more productive, faster, and better at accomplishing their tasks.
- **We're informed by empathy.** We’re human, and we design for humans, so we strive for understanding, self-awareness, and connection. We are quirky, and we introduce our quirks into designs when appropriate.
- **When we find problems that are simple to fix, we are empowered to make those changes ourselves.** If a change will take you less than 15 minutes to make (for example, a minor change to our website or microcopy in the product), then start with an MR instead of an issue. By making the change yourself, you are taking immediate action to improve our product, and you might learn a new skill, too! If it seems simple, but you have questions, remember that there are people who can help you with code changes both in the UX department and across the company. (Even Sid is willing to help, if you need it.)

Learn more about [how we work](/handbook/engineering/ux/how-we-work/) within our department and with cross-functional partners.

## Stage group UX strategy

See the Team Structure and strategic UX direction for stage groups in the [Product Design](/handbook/engineering/ux/product-design/) handbook page.

## Personas we use

Existing personas are documented within the [handbook](/handbook/marketing/strategic-marketing/roles-personas/).

New personas or updates to existing personas can be added at any time.

Personas should be:

- Informed by research.
- Driven by job title or feature.
- Gender neutral.

## Areas of Responsibility

- **Pajamas Design System:** To ensure that everyone can contribute to GitLab with confidence we provide everyone with the right resources and know-how. The [Pajamas](https://design.gitlab.com/) design system is the single source of truth for everything anyone needs to know about contributing to GitLab. The UX Department owns the visual and interaction design, as well as implementation of Pajamas.
- **Navigation of GitLab UI:** Navigation is an extremely important part of the user experience. Our goal is keeping the navigation architecture intelligible, comprehensible and making sure it serves every user need. [Learn how to make changes to navigation](/handbook/engineering/ux/navigation/).
- **System Usability Scale:** We measure the usability of our product with the industry-standard [System Usability Scale (SUS)](/handbook/engineering/ux/performance-indicators/system-usability-scale/) methodlogy. Our UX Research team is responsible for defining our process and running the survey once per quarter, but all of UX and Product Management are responsible for attaining an [industry-leading score](/company/strategy/#2-build-on-our-open-core-strength).
- **UX Scorecards:** As we grow our platform, we want to keep evaluating user experience of various user tasks and flows to make sure we are tracking progress and improvements over time. [UX Scorecards](/handbook/engineering/ux/ux-scorecards/) is our framework for achieving this goal.
- **Category Maturity Scorecards:** We listen to our users and grade the maturity of our product based on user performance and feedback. [Category Maturity Scorecards](/handbook/engineering/ux/category-maturity-scorecards/) is the methodology we use for these evaluations.
- **Technical Documentation:** Our users need reliable documentation, as it helps keep track of all aspects of a platform and it improves on the quality of a software product. We manage [docs.gitlab.com](https://docs.gitlab.com/) as well as related processes and tooling.
- **First Look:** Inviting users into everything we do is very important to us in order to be able to collect feedback. [First Look](https://about.gitlab.com/community/gitlab-first-look/) is our user engagement and recruiting program, which enables us to connect with our users and get their thoughts on our product.

* **UX Showcase:** Collaboration is one of our values. <%= data.ux_showcase.about %>

## FY22 direction

Continuing our focus in fiscal year 2021 (FY21), the UX direction for FY22 is still to offer a best-in-class user experience for all of the DevOps categories in which we compete. The difference this year is that we intend to tie those improvements to two industry standards:

- McKinsey’s five-year study on [The business value of design](https://www.mckinsey.com/business-functions/mckinsey-design/our-insights/the-business-value-of-design) that resulted in the McKinsey Design Index (MDI)
- Continued focus on improving our [System Usability Scale score](/handbook/engineering/ux/performance-indicators/#perception-of-system-usability)

### The business value of design

> “Top-quartile MDI scorers increased their revenues and total returns to shareholders (TRS) substantially faster than their industry counterparts did over a five-year period—-**32 percentage points higher revenue growth and 56 percentage points higher TRS growth for the period as a whole.**” - McKinsey & Company Report on [The business value of design](https://www.mckinsey.com/business-functions/mckinsey-design/our-insights/the-business-value-of-design)

In a five-year study of over 300 publicly listed companies, McKinsey uncovered four MDI themes correlated with improved financial performance:

- **Analytical leadership** that measures and drives design performance with the same rigor as revenues and costs.
- **Cross-functional talent** that makes user-centric design everyone’s responsibility.
- **Continuous iteration** that de-risks development through continual listening, testing, and iterating with end users.
- **User experience** that breaks down internal walls between physical, digital, and service design.

As UX practitioners, we deeply care about creating a great product experience, because we know that it improves the day-to-day lives of our end users. What we tend to forget is that great UX also meaningfully improves our company’s financial health. A huge part of our role is to advocate for beautiful design and effective design practices. Tying this advocacy back to revenue impact can be a really effective way to help the company make important prioritization decisions.

### System usability

We’ve been [tracking](/handbook/engineering/ux/performance-indicators/#perception-of-system-usability) our [System Usability Scale](/handbook/engineering/ux/performance-indicators/system-usability-scale/) score since Q1 of FY20, so we now have a long-term view into how users perceive our product experience. While our score is above average, it has declined quarter over quarter.

Fortunately, we have the support of our executive team in giving usability improvements the appropriate focus, with a target of [raising it to 80](/company/strategy/#2-build-on-our-open-core-strength) by the end of FY24. We also have significant user research that indicates where we should focus our efforts to have the greatest impact.

### Actionable steps

In alignment with the MDI themes and feedback from user research, the following is a list of where our UX team will focus in FY22 to improve our SUS score. Often, but not always, we will use this list as a starting place for our quarterly OKRs.

It’s important to note that this list isn’t exhaustive. Instead, we've focused on areas that the UX team can achieve independently, while we still continue to collaborate with our product development partners on other company priorities.

| MDI Theme | Goal | DRI | Epic or Issue Link |
| --------- | ---- | --- | ------------------ |
| User experience | Document the user-validated [Jobs to be Done (JTBD)](/handbook/engineering/ux/jobs-to-be-done/) for every actively maturing product category to ensure that we’re focused on the goals our users want to accomplish. | Valerie Karnes | [Q1 issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10463)] |
| User experience | Improve our documentation while still supporting the [Development Department Narrow MR Rate](/handbook/engineering/development/performance-indicators/#development-department-narrow-mr-rate) by refining content post merge. | Susan Tacker | TBD |
| User experience | Improve the clarity and consistency of our UI text by engaging Technical Writers early and often. | Craig Norris | [Q1 issue](https://gitlab.com/gitlab-org/technical-writing/-/issues/352) |
| User experience | Perform cross-stage research that focuses on identifying key cross-stage moments and how we can improve that experience to encourage adoption and reduce friction to drive cross-stage usage [SPU](https://about.gitlab.com/handbook/product/performance-indicators/#stages-per-user-spu) | Lorie Whitaker | [link to issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10466) |
| User experience | Understand expectations users have from a SaaS DevOps platform in contrast to the one that is self managed. | Adam Smolinski | TBD |
| Analytical leadership | Measure the new user experience for stages the Growth team has prioritized, and document this process to enable other stages to reproduce it, with the goal of more quickly identifying and addressing gaps. | Jacki Bauer | TBD |
| Analytical leadership | Now that we have [Actionable Insights](/handbook/engineering/ux/performance-indicators/#actionable-insights) in place, make sure we’re doing something with them. | Jeff Crow | TBD |
| Analytical leadership | Burn down UX debt and shorten time to close. | Valerie Karnes | TBD |
| Analytical leadership | Make [UX performance indicators](/handbook/engineering/ux/performance-indicators/) a bigger focus by reviewing them more frequently and taking action where our KPIs are under target. | Christie Lenneville | TBD |
| Cross-functional talent | More actively engage Product Managers and Developers in UX research by making insights easier to consume. | Katherine Okpara | [Q1 issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10467) |
| Cross-functional talent | Ensure that [Pajamas](https://design.gitlab.com/) is not a catch-all solution to all design problems and that stage groups contribute, as well as work outside of it when warranted. | Taurie Davis | TBD |
| Cross-functional talent | Work with Development to implement remaining Pajamas components into the GitLab product. | Taurie Davis | [Phase 2](https://gitlab.com/gitlab-org/gitlab/-/issues/322510) |
| Cross-functional talent | Create guidelines for what an MR review should look like from a UX perspective. | Marcel van Remmerden | TBD |
| Continuous iteration | Improve [UX KPI of Problem/Solution validation](/handbook/engineering/ux/performance-indicators/#proactive-ux-work) to hit target of 2/designer/quarter. | Andrej Kiripolsky | [link to issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10506) |
| Continuous iteration | Include more competitor analysis in our iterative design work. | [Mike Long](https://gitlab.com/mikelong) | [Q1 heuristics issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10464) |
| Continuous iteration | Increase velocity by encouraging designers to stay in low fidelity for small changes in which we have high confidence. | Valerie Karnes | TBD |
| Continuous iteration | Pilot a process to iteratively test SUS-specific improvements. | Anne Lasch | [link to issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10465) |

## Meet Some of Our Team Members

This section is inspired by the recent trend of Engineering Manager READMEs. _e.g.,_ [Hackernoon: 12 Manager READMEs (from some of the best cultures in tech)](https://hackernoon.com/12-manager-readmes-from-silicon-valleys-top-tech-companies-26588a660afe). Get to know more about the people on our team!

- [Christie Lenneville](/handbook/engineering/ux/one-pagers/christie-readme/) - VP of User Experience
- [Valerie Karnes](https://gitlab.com/vkarnes/readme) - Director of Product Design
- [Sarah Jones](/handbook/engineering/ux/one-pagers/sarahod-readme/) - UX Research Manager
- [Jacki Bauer](https://gitlab.com/jackib/jacki-bauer/blob/master/README.md) - Product Design Manager
- [Justin Mandell](https://gitlab.com/jmandell/readme) - Product Design Manager
- [Taurie Davis](https://gitlab.com/tauriedavis/readme/blob/master/README.md) - Product Designer Manager
- [Rayana Verissimo](https://gitlab.com/rverissimo/readme) - Product Design Manager
- [Matej Latin](https://gitlab.com/matejlatin/focus) - Sr. Product Designer
- [Iain Camacho](https://gitlab.com/icamacho/koda/blob/master/README.md) - Sr. Product Designer
- [Jeremy Elder](https://gitlab.com/jeldergl/view/blob/master/README.md) - Sr. Product Designer, Visual Design
- [Nadia Sotnikova](https://gitlab.com/nadia_sotnikova/tasks/blob/master/README.md) - Product Designer
- [Austin Regnery](https://gitlab.com/aregnery/dear-journal/-/blob/master/README.md) - Product Designer
- [Anne Lasch](https://gitlab.com/alasch/about-anne/-/blob/master/README.md) - Sr. User Experience Researcher

[ux-guide]: https://docs.gitlab.com/ee/development/ux_guide/
[ux-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX
[ux-ready-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX+ready
[gitlab-design-project-readme]: https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md
[twitter-sheet]: https://docs.google.com/spreadsheets/d/1GDAUNujD1-eRYxAj4FIYbCyy8ltCwwIWqVTd9-gf4wA/edit
[everyone-designer]: https://library.gv.com/everyone-is-a-designer-get-over-it-501cc9a2f434
[pajamas]: https://design.gitlab.com

### Time zones

Our team members collaborate across multiple time zones. You can use the [UX Department timezone.io](https://timezone.io/team/gitlab-design) team page to check what hour it is in each of the locations of our team members.

## Join our UX Team

Are you interested in joining our team or hearing about new roles that open up within our department?

- [Product Design](https://boards.greenhouse.io/gitlab/jobs/4974496002)
- [Tech Writing](https://boards.greenhouse.io/gitlab/jobs/4983530002)
- [UX Research](https://boards.greenhouse.io/gitlab/jobs/4983498002)
