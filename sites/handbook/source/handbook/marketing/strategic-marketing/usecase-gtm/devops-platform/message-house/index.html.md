---
layout: markdown_page
title: "DevOps Platform Message House"
---



| Positioning Statement: | *An Open DevOps Platform to visualize and optimize workflows, collaborate across teams and projects, and deliver results faster and more securely.* |
|------------------------|-------------------------------------------------------------------------|
| **Short Description** | An Open DevOps Platform removes operational overhead, enables seamless collaboration, and allows businesses to deliver results faster, more securely, and more efficiently. |
| **Long Description** | GitLab is a complete, Open DevOps Platform deployed as a single application, increasing transparency, enabling collaboration, and increasing the quality, velocity, and security of business results. GitLab eliminates the overhead and cost of integrating and managing complex toolchains, allowing you to focus on creating business value. One underlying data store enables end-to-end transparency so every stakeholder—from portfolio managers to developers to security experts to customers—can collaborate in the development process and understand exactly what and why changes were made, by whom, and how they affected product quality, security, and compliance. One application means greater efficiency, better security, higher quality, and more value delivered for the same time and cost. |


| **Key-Values** | Value 1: | Value 2: | Value 3: |
|--------------|----------|----------|----------|
| **Promise** | **Efficiency**:<br>Focus on value—add, not infrastructure, with a single application to meet all your DevOps needs. | **Deliver Better Products Faster**:<br>Iterate faster and innovate together throughout the product lifecycle. | **Security and Compliance**:<br>Increase security and reduce compliance risk with built-in scanning and end-to-end traceability. |
| **Pain points** |  - Wasted time and resources maintaining toolchain integrations<br> - Silos of tool-specific competencies<br> - "Data gaps" in integrations resulting in incomplete context and manual workarounds<br> - Expensive to license and support | - Poor velocity and business agility<br> - Struggle to measure efficiency and understand where processes break down<br> - Difficulty moving from analysis to action<br> - correlating process changes with efficiency outcomes<br> - lack of consistent measurement across projects and teams | - Unsatisfying trade-offs<br> - Remediating vulnerabilities late in the development process is costly and time-consuming<br> - forensics and audits create an enormous burden across the entire team<br> - Difficulty scaling app sec to DevOps iterations and velocity |
| **Why GitLab** | A single application eliminates the need to maintain brittle integrations<br> A shared data store allows any user in any stage of the development process to access data from any other stage, improving efficiency via collaboration and transparency<br> Security and Compliance policies can be automated and applied consistently across projects and from end-to-end in the SDLC. | - As an end-to-end Open DevOps platform built on a single data store, GitLab provides complete visibility into all of your work, including requirements creation, planning, code changes, security and quality reviews, deplopyments, and all collaboration throughout the process. This enables out-of-the-box analytics that can be customized to your workflows, and a variety of dashboards that surface relevant information to specific roles within your organization, while allowing everyone to actively contribute with just a few clicks. | - Application security scans run seamlessly with each commit, allowing the developer to find and fix them early and within their workflow.<br> - The infrastructure required of modern apps is monitored and protected<br> - The Security Dashboard provides insights security pros need, showing remaining vulnerabilities across projects and/or groups, along with actions taken, by whom and when.<br> - All actions, from planning to code changes to approvals, are captured and correlated for easy traceability during audit events. |


| **Proof points** | *(list specific analyst reports, case studies, testimonials, etc)*  |

- [Glympse case study](/customers/glympse/)
(~20 tools consolidated into GitLab; Glympse remediated security issues faster than any other company in their Security Auditor's experience)

> "Development can move much faster when engineers can stay on one page and click buttons to release auditable changes to production and have easy rollbacks; everything is much more streamlined. Within one sprint, just 2 weeks, Glympse was able to implement security jobs across all of their repositories using GitLab’s CI templates and their pre-existing Docker-based deployment scripts."<br>**Zaq Wiedmann**<br>Lead Software Engineer, Glympse

***

- [BI Worldwide case study](/customers/bi_worldwide/)
(BI Worldwide performed 300 SAST/Dependency scans in the first 4 days of use, helping the team identify previously unidentified vulnerabilities.)

> "One tool for SCM+CI/CD was a big initial win. Now wrapping security scans into that tool as well has already increased our visibility into security vulnerabilities. The integrated Docker registry has also been very helpful for us. Issue/Product management features let everyone operate in the same space regardless of role."<br>Adam Dehnel<br>Product architect, BI Worldwide

***

- [Gartner 2020 Market Guide for DevOps Value Stream Delivery Platforms](https://page.gitlab.com/resources-report-gartner-market-guide-vsdp.html)

***
